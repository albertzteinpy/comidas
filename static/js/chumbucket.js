  function articulosaved(datos){
    $('#id_id').val(datos.pk);
    toastr['success'](datos.mesagge,'');
  }


function sendit(e){
	e.preventDefault();
	//var data = $(this).serializeArray();
	var url = $(this).attr('action');
	var type = $(this).attr('method');
	forma = $(this).get(0);
	//forma = $('#empresaspt1').get(0);
	
	var data = new FormData(forma);
	$.ajax({url:url,
			type:type,
			data:data,
			cache: false,
	        processData: false,
	        contentType:false,
    	    dataType: 'json',
			success:function(response){
				$('.errr').removeClass('errr');
				$('.alert').remove();
				if(response.errors){

					$.each(response.errors,function(err,i){

						$('#id_'+err).addClass('errr').after('<div  class="id_'+err+' alert alert-warning alert-dismissible" rol="alert"><i class="glyphicon glyphicon-fire"></i> ('+err+')  '+i+'</div>');
					$('.errr').change(function(){
						$('.id_'+err+'').remove();
					});

					});
					
					$('.errr:first').focus();

				}
				if(response.saved=='ok'){
					if(response.callback){
						if(typeof(window[response.callback])=='function'){
							window[response.callback](response.datos);
						}
					}

				}
			}

	});

}