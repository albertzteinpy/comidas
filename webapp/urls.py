# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from webapp import views

urlpatterns = [

    url(r'^$', views.IndexView, name='inicio'),
    url(r'^formula/$', views.FormulaView, name='inicio'),
    url(r'^comida/$', views.ComidaView, name='inicio'),
    #url(r'^getarticulos/$', views.getArtView, name='getarticulos'),

]
