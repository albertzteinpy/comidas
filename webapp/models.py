# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.db import models



class Caloricgroup(models.Model):
	calorics = models.CharField(u'Caloric',max_length=200)
	fruits = models.CharField(u'Fruits',max_length=100)
	vegetables = models.CharField(u'vegetables',max_length=100)
	cereals = models.CharField(u'cereals',max_length=100)
	vegfat = models.CharField(u'Fat',max_length=100)
	proteins = models.CharField(u'proteins',max_length=100)

	def __unicode__(self):
		return 'Grupo %s'%(self.calorics)


YAVES = [
			('cero','cero'),
			('1','Carbohidratos (Vegetales y Verduras)'),
			('2','Carbohidratos (Frutas)'),
			('3','Carbohidratos (Almidones, Cereales y Granos)'),
			('4,','Grasas Vegetales'),
			('5','Proteínas (Vegetal)'),
			('6','Proteína (Animal)'),

]


class Alimento(models.Model):
	yave = models.CharField(u'key_macronutriente',max_length=200,choices=YAVES)
	aliemnto = models.CharField(u'alimento',max_length=200)
	porcion = models.CharField(u'porcion',max_length=20)

	def __unicode__(self):
		yavex = '%s'%self.yave
		return '%s --- %s'%(YAVES[int(yavex)][1],self.aliemnto)