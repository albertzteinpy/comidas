# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from comidas.forms import *
from webapp.models import *
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.sites.shortcuts import get_current_site
from webapp.forms import *
import simplejson



class IndexView(View):
    """View to retrieve companies list."""

    template = 'webapp/home.html'

    def get(self, request):
        forma = Qone
    	context = {}
    	current_site = 1
        context['forma']=forma
		#myapp = apps.get_app_config(current_site.name)

        context['current_site'] =1



        response = render(request, self.template, context)
        return response

IndexView = IndexView.as_view()


class FormulaView(View):
    """View to retrieve companies list."""

    template = 'webapp/home.html'

    def post(self, request):

        FORMULAS = {
                    'm':{
                        'fa':[14.7,469],
                        'sa':[8.7,829]
                    },
                    'h':{
                        'fa':[15.3,679],
                        'sa':[11.6,879]         
                    }

        }


        OB = {
                '0':[0,0],
                'lv':[300,600],
                'md':[600,1000],
                'sv':[1000,5000],
                

            } 

        data = request.POST.copy()
        args = {}
        y = {}
        context = RequestContext(request)
        sa = 'index.html'
        forma = Qone(data)
        if forma.is_valid():
            indicess = FORMULAS[data['genre']][data['age']]
            w = float(data['weight'])
            al = float(data['activity_level'])
            ob = OB[data['obesity']][0]
            fermula1 = (((indicess[0]*w) + indicess[1]) * al)-ob
            group = round(fermula1,-2)
            cg = Caloricgroup.objects.get(calorics=int(group))
            #response = model_to_dict(cg)

            response = {'gc':int(group)}

            return HttpResponse(simplejson.dumps(response))
        else:
            response = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(response))

FormulaView = FormulaView.as_view()


class ComidaView(View):
    """View to retrieve companies list."""

    template = 'webapp/comidas.html'
    macros = [('0','Carbohidratos (Vegetales y Verduras)'),
            ('1','Carbohidratos (Frutas)'),
            ('2','Carbohidratos (Almidones, Cereales y Granos)'),
            ('3','Grasas Vegetales'),
            ('4','Proteínas (Vegetales y Animales)'),
            ]

    def  get(self, request):
        gc = request.GET.get('gc',None)
        tiempos = ('desayuno','colacion1','comida','colacion2','cena')
        t = request.GET.get('t',0)
        context = {}
        if gc:
            cg = Caloricgroup.objects.get(calorics=int(gc))


            context['vegetables'] = simplejson.loads(cg.vegetables)
            context['fruits'] = simplejson.loads(cg.fruits)
            context['cereals'] = simplejson.loads(cg.cereals)
            context['vegfat'] = simplejson.loads(cg.vegfat)
            context['proteins'] = simplejson.loads(cg.proteins)
            context['t']='webapp/%s.html'%tiempos[int(t)]
            context['gc']=gc
            alimentos = Alimento.objects.all()
        
            gp1 = alimentos.filter(yave=2).order_by('?')[:5]
            context['afruits']=[{'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk} for x in gp1]
            gp2 = alimentos.filter(yave=1).order_by('?')[:5]
            context['avegetables']=[{'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk} for x in gp2]
            gp3 = alimentos.filter(yave=3).order_by('?')[:5]
            context['acereals']=[{'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk} for x in gp3]
            gp4 = alimentos.filter(yave=4).order_by('?')[:5]
            context['avegfat']=[{'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk} for x in gp4]


        gp1 = alimentos.filter(yave=5).order_by('?')[:2]
        context['aproteins']=[{'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk} for x in gp1]

        gp1 = alimentos.filter(yave=6).order_by('?')[:1]
        for x in gp1:
            context['aproteins'].append({'alimento':'%s'%x.aliemnto,'porcion':x.porcion,'key':x.pk})

            tiempos_sel = ('Desayuno',u'Colación 1','Comida',u'Colación 2','Cena')
            context['comidas'] = tiempos_sel
            context['macros']=self.macros
            context['timepo_select'] = tiempos_sel[int(t)]
        response = render(request, self.template, context)
        return response

ComidaView = ComidaView.as_view()

