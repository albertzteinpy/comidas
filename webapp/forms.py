# -*- encoding: utf-8 -*-

from __future__ import unicode_literals
from django import forms



GENRES = [
            ('m','Mujer'),
            ('h','Hombre'),

]


ACTIVS = [

            ('1.2','Enfermedad'),
            ('1.3','Oficinistas, estudiantes, maestros, secretarias'),
            ('1.6','Electricistas, industria liegera, personal de limpieza, deporte leve'),
            ('1.5','Enfermeras, industria leve, personal de limpieza, deporte leve'),
            ('1.7','Obreros activos, albañiles, carpinteros, plomeros, pintores, repartidores'),
            ('1.60','Deportistas (no de alto rendimiento)'),
]

AGES = [
        ('fa','18-30'),
        ('sa','31-60'),
]


OB = [
        ('0','Ninguno'),
        ('lv','Leve'),
        ('md','Moderada'),
        ('sv','Severa'),        
]



class Qone(forms.Form):
    genre = forms.ChoiceField(label='Genero',widget=forms.Select(attrs={'class':'form-control'}),choices=GENRES)  
    age = forms.ChoiceField(label='Edad',widget=forms.Select(attrs={'class':'form-control'}),choices=AGES)  
    weight = forms.CharField(label='Peso',widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Kg'}))  
    activity_level = forms.ChoiceField(label='Tipo de actividad',widget=forms.Select(attrs={'class':'form-control'}),choices=ACTIVS)  
    obesity = forms.ChoiceField(label='¿Obesidad?',widget=forms.Select(attrs={'class':'form-control'}),choices=OB)  
